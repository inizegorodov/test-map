import axios from "axios";

/** Общий инстанс axios для всех запросов*/
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API || 'https://nominatim.openstreetmap.org/', // можно задать базовый url
  timeout: 60000 
});

/** Перехватчик запроса*/
service.interceptors.request.use(
  config => {
    // Это для примера. Тут можно дополнить запрос, например, токеном или иными заголовками
    return config;
  },
  error => {
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

/** Перехватчик ответа*/
service.interceptors.response.use(

  response => {
    // Это для примера. Тут можно написать логику, характерную для какого-то особого ответа. 
    return response;
  },
  error => {
    alert('Server error') // тут можно красивое уведомление сделать и брать текст из ошибки, если сервер будет возвращать

    return Promise.reject(error);
  }
);

export default service;
