import request from "@/utils/request";


/**
  * Получение списка геообъектов
  * @param path - url поиска
  * @param query - параметры запроса
*/
export function fetchList({ path = '/search', query = {} }) {
  return request({
    url: path,
    method: "get",
    params: query
  });
}
