import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import { getToken as checkAuth } from "@/utils/auth";


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})


router.beforeEach(async (to, from, next) => {
  
  const isAuth = checkAuth();
  
  if (!isAuth && to.path !== '/login') {
    next({path: '/login'})
  }

  if (isAuth && to.path === '/login') {
    next({path: '/'})
  }

  next();

});

export default router
