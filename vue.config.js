module.exports = {
  configureWebpack: {
    devServer: {
      proxy: {
         '^/search': {
          target:  'https://nominatim.openstreetmap.org/',
          ws: false,
          changeOrigin: true
        },

      }

    },

  },
};
